from objects import *
import sys

file = open(sys.argv[1]).read()
inp = file.split("\n")
robot = inp[0].split()
if not (0.05 <= float(robot[0]) <= 0.15 and 0 <= float(robot[3]) <= 2*3.14):
    raise TypeError
obj_amount = inp[1].split()
boxes = []
m_obstacles = []
s_obstacles = []
line = 2
for i in range(int(obj_amount[0])):
    boxes.append(inp[line].split())
    line += 1
for i in range(int(obj_amount[1])):
    m_o = inp[line].split()
    if not (float(robot[0]) <= float(m_o[2]) <= float(robot[0])*1.5):
        raise TypeError
    m_obstacles.append(m_o)
    line += 1
for i in range(int(obj_amount[2])):
    s_obstacles.append(inp[line].split())
    line += 1

start = datetime.now()
while True:
    try:    
        object_list = []
        rbt = Robot(
            float(robot[0]),
            float(robot[1]),
            float(robot[2]),
            float(robot[3])
            )

        for box in boxes:
            object_list.append(
                Box(
                    float(box[0]),
                    float(box[1]),
                    float(robot[0]),
                    float(box[2]), 
                    float(box[3])
                    )
                )

        for m_obs in m_obstacles:
            object_list.append(
                MovableObstacle(
                    float(m_obs[0]),
                    float(m_obs[1]),
                    float(m_obs[2])
                    )
                )

        for s_obs in s_obstacles:
            object_list.append(
                StaticObstacle(
                    float(s_obs[0]),
                    float(s_obs[1]),
                    float(s_obs[2]),
                    float(s_obs[3])
                    )
                )

        world = World(0, 1, rbt, object_list, sys.argv[2])
        file = open(sys.argv[2], "w")
        file.close()
        world.find_path()
        world.move_robot()
        world.finish()
        duration = datetime.now() - start
        print("Algorithm takes ", duration, " to finish the problem.")
        break
    except KeyboardInterrupt:
        print("Interrupted.")
        break
    except RuntimeError:
        continue
