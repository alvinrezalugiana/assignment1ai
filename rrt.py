import random
import math
import copy


class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None

class RRT:
    def __init__(self, obj, width, obstacle_list, world, distance):
        self.start = obj.pos
        self.goal = obj.goal
        self.spare = width
        self.obstacles = obstacle_list
        self.min_world = world.min_range + self.spare
        self.max_world = world.max_range - self.spare
        self.distance = round(math.sqrt((distance**2)*2)*1.5, 3)
        self.sample_rate = 50
        self.node_list = []
        self.checker = CollisionChecker(obj, self.spare)

    def plan(self):
        self.node_list = [self.start]
        while True:
            # Do random sampling
            if random.randint(0,100) > self.sample_rate:
                rand = [
                    random.uniform(self.min_world, self.max_world),
                    random.uniform(self.min_world, self.max_world)
                    ]
            else: rand = [self.goal.x, self.goal.y]
            # Find the nearest existing point
            nearest_node, idx = self.find_nearest(self.node_list, rand)
            # Expand the path
            theta = math.atan2(rand[1] - nearest_node.y, rand[0] - nearest_node.x)
            new_node = copy.deepcopy(nearest_node)
            # Check if there is collision
            new_node.x = round(new_node.x + self.distance * math.cos(theta), 3)
            new_node.y = round(new_node.y + self.distance * math.sin(theta), 3)
            new_node.parent = idx
            if not self.checker.is_colliding(new_node, self.obstacles):
                self.node_list.append(new_node)
                # Check if it is a goal
                dx = new_node.x - self.goal.x
                dy = new_node.y - self.goal.y
                if math.sqrt(dx**2 + dy**2) <= self.distance:
                    print("Goal found!")
                    break
        path = [self.goal]
        path_idx = len(self.node_list) - 1
        while self.node_list[path_idx].parent is not None:
            node = self.node_list[path_idx]
            path.append(node)
            path_idx = node.parent
        path.append(self.start)
        real_path = self.generate_move(path)
        return path, real_path

    def generate_move(self, path):
        final = []
        tmp_path = path[::-1]
        for idx in range(len(tmp_path)-1):
            no_spare = True if (idx == 0 or idx == len(tmp_path)-2) else False
            pos = Node(tmp_path[idx].x, tmp_path[idx].y)
            goal = Node(tmp_path[idx+1].x, tmp_path[idx+1].y)
            move_axis = "x" if abs(goal.x-pos.x) <= abs(goal.y-pos.y) else "y"
            i = 0
            err_count = 0
            while pos.x != goal.x or pos.y != goal.y:
                if move_axis == "x":
                    if abs(goal.x-pos.x) < 0.001:
                        pos.x = goal.x
                        move_axis = "y"
                    elif goal.x-pos.x > 0:
                        pos.x = round(pos.x + 0.001, 3)
                    elif goal.x-pos.x < 0:
                        pos.x = round(pos.x - 0.001, 3)
                else:
                    if abs(goal.y-pos.y) < 0.001:
                        pos.y = goal.y
                        move_axis = "x"
                    elif goal.y-pos.y > 0:
                        pos.y = round(pos.y + 0.001, 3)
                    elif goal.y-pos.y < 0:
                        pos.y = round(pos.y - 0.001, 3)
                if self.checker.is_colliding(pos, self.obstacles, no_spare):
                    if err_count >= 10:
                        print("Multiple collisions found, trying to find different path...")
                        raise RuntimeError
                    err_count += 1
                    if move_axis == "x":
                        if goal.x-pos.x > 0:
                            pos.x = round(pos.x - 0.001, 3)
                        elif goal.x-pos.x < 0:
                            pos.x = round(pos.x + 0.001, 3)
                        move_axis = "y"
                    elif move_axis == "y":
                        if goal.y-pos.y > 0:
                            pos.y = round(pos.y - 0.001, 3)
                        elif goal.y-pos.y < 0:
                            pos.y = round(pos.y + 0.001, 3)
                        move_axis = "x"
                else: final.append(Node(pos.x, pos.y))
                i += 1
        return final

    def get_total_distance(self, path):
        dis = 0
        for i in range(len(path) - 1):
            dx = path[i+1][0] - path[i][0]
            dy = path[i+1][1] - path[i][1]
            d = math.sqrt(dx**2 + dy**2)
            dis += d
        return dis

    def smooth_path(self, path, obstacles):
        dis = self.get_total_distance(path)

    def find_nearest(self, node_list, rand):
        d_list = [(node.x - rand[0]) ** 2 + (node.y - rand[1])
                 ** 2 for node in node_list]
        min_index = d_list.index(min(d_list))
        return node_list[min_index], min_index

class CollisionChecker:

    def __init__(self, obj, width):
        self.obj = obj
        self.width = width
        self.spare = self.width

    def is_colliding(self, node, obstacles, no_spare=False):
        self.spare = self.width/2 if no_spare and not self.obj.is_robot else self.width
        for obs in obstacles:
            if self.obj.is_robot and not obs.is_obstacle: # The object is robot
                self.spare = 0
            if obs.is_movable: # The object is square
                left = round(obs.pos.x-(obs.width/2)-self.spare, 3)
                right = round(obs.pos.x+(obs.width/2)+self.spare, 3)
                lower = round(obs.pos.y-(obs.width/2)-self.spare, 3)
                upper = round(obs.pos.y+(obs.width/2)+self.spare, 3)
            elif not obs.is_movable: # The object is rectangle (static obstacle)
                left = round(obs.pos.x-(obs.x_length/2)-self.spare, 3)
                right = round(obs.pos.x+(obs.x_length/2)+self.spare, 3)
                lower = round(obs.pos.y-(obs.y_length/2)-self.spare, 3)
                upper = round(obs.pos.y+(obs.y_length/2)+self.spare, 3)
            if self.obj.has_goal and left < node.x < right and lower < node.y < upper:
                return True
            elif self.obj.is_robot:
                x = [self.obj.get_x1(node.x), node.x, self.obj.get_x2(node.x)]
                y = [self.obj.get_y1(node.y), node.y, self.obj.get_y2(node.y)]
                for i in range(3):
                    if left < x[i] < right and lower < y[i] < upper:
                        return True
            if (obs.has_goal and obs.goal_found): # Check if the path collides with other box's goal
                left = round(obs.goal.x-(obs.width/2)-self.spare, 3)
                right = round(obs.goal.x+(obs.width/2)+self.spare, 3)
                lower = round(obs.goal.y-(obs.width/2)-self.spare, 3)
                upper = round(obs.goal.y+(obs.width/2)+self.spare, 3)
                if self.obj.has_goal and left < node.x < right and lower < node.y < upper:
                    return True
                elif self.obj.is_robot:
                    x = [self.obj.get_x1(node.x), node.x, self.obj.get_x2(node.x)]
                    y = [self.obj.get_y1(node.y), node.y, self.obj.get_y2(node.y)]
                    for i in range(3):
                        if left < x[i] < right and lower < y[i] < upper:
                            return True
        return False
