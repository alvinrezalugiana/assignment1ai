from rrt import *
from datetime import datetime

class World:
    def __init__(self, min_range, max_range, robot, obj_list, filename):
        self.min_range = min_range
        self.max_range = max_range
        self.robot = robot
        self.obj_list = obj_list
        width_list = []
        for obj in self.obj_list:
            if obj.is_movable:
                width_list.append(obj.width)
            else:
                width_list.append(min(obj.x_length, obj.y_length))
        self.max_dist = min(width_list)
        self.path_text = ""
        self.filename = filename

    def find_path(self):
        for obj in self.obj_list:
            if obj.is_movable and not obj.is_obstacle:
                tmp_obs = self.obj_list[:]
                tmp_obs.remove(obj)
                path = RRT(obj, obj.width, tmp_obs, self, self.max_dist)
                p, obj.path = path.plan()
                obj.goal_found = True

    def move_robot(self):
        for obj in self.obj_list:
            if obj.is_movable and not obj.is_obstacle:
                r_pos, self.robot.goal = self.decide_direction(obj)
                robot_path = RRT(self.robot, self.robot.width, self.obj_list, self, self.max_dist)
                p, self.robot.path = robot_path.plan()
                while self.robot.path != []: # move the robot to init position
                    pos = self.robot.path.pop(0) # to push the box
                    self.robot.pos = pos
                    self.create_path_in_text()
                self.rotate(self.robot, r_pos)
                self.touch_object(self.robot, obj, r_pos)
                change = False
                while obj.path != []:
                    if change:
                        self.change_direction(self.robot, obj, r_pos, tmp_pos)
                        r_pos = tmp_pos
                        change = False
                    if len(obj.path) > 1:
                        tmp_pos, goal = self.decide_direction(obj, r_pos, self.robot.goal)
                        if r_pos != tmp_pos:
                            change = True
                    ## robot position to be configured here
                    obj.pos = obj.path.pop(0)
                    if r_pos == "left":
                        self.robot.pos = obj.get_left_side()
                        self.robot.o = 1.571
                    elif r_pos == "right":
                        self.robot.pos = obj.get_right_side()
                        self.robot.o = 1.571
                    elif r_pos == "down":
                        self.robot.pos = obj.get_down_side()
                        self.robot.o = 0
                    elif r_pos == "up":
                        self.robot.pos = obj.get_up_side()
                        self.robot.o = 0
                    self.create_path_in_text()

    def decide_direction(self, obj, r_pos=None, goal=None):
        dis_x = round(obj.path[1].x - obj.path[0].x, 3)
        dis_y = round(obj.path[1].y - obj.path[0].y, 3)
        if  dis_x > 0: # move to the right
            left = round(obj.pos.x-(obj.width/2)-(self.robot.width/2), 3)
            goal = Node(left, obj.pos.y) # get to the left
            r_pos = "left"
        elif  dis_x < 0: # move to the left
            right = round(obj.pos.x+(obj.width/2)+(self.robot.width/2), 3)
            goal = Node(right, obj.pos.y) # get to the right
            r_pos = "right"
        elif  dis_y > 0: # move up
            down = round(obj.pos.y-(obj.width/2)-(self.robot.width/2), 3)
            goal = Node(obj.pos.x, down) # get to the bottom
            r_pos = "down"
        elif  dis_y < 0: # move down
            up = round(obj.pos.y+(obj.width/2)+(self.robot.width/2), 3)
            goal = Node(obj.pos.x, up) # get to the above
            r_pos = "up"
        return r_pos, goal

    def change_direction(self, robot, obj, init_dir, target_dir):
        cur_dir = False
        if target_dir == "left":
            target = obj.get_left_side()
            if init_dir == "up":
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "x"
                i = 0.001
            elif init_dir == "down":
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "x"
                i = -0.001
            elif init_dir == "right":
                target = obj.get_up_side()
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "y"
                i = 0.001
                cur_dir = "up"
        elif target_dir == "right":
            target = obj.get_right_side()
            if init_dir == "up":
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "x"
                i = -0.001
            elif init_dir == "down":
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "x"
                i = 0.001
            elif init_dir == "left":
                target = obj.get_up_side()
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "y"
                i = -0.001
                cur_dir = "up"
        elif target_dir == "up":
            target = obj.get_up_side()
            if init_dir == "left":
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "y"
                i = -0.001
            elif init_dir == "right":
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "y"
                i = 0.001
            elif init_dir == "down":
                target = obj.get_right_side()
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "x"
                i = 0.001
                cur_dir = "right"
        elif target_dir == "down":
            target = obj.get_down_side()
            if init_dir == "left":
                target_rot = round(robot.o + 1.571, 3)
                what_to_move = "y"
                i = 0.001
            elif init_dir == "right":
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "y"
                i = -0.001
            elif init_dir == "up":
                target = obj.get_right_side()
                target_rot = round(robot.o - 1.571, 3)
                what_to_move = "x"
                i = -0.001
                cur_dir = "right"
        x = 0.001 if target.x-robot.pos.x >= 0 else -0.001
        y = 0.001 if target.y-robot.pos.y >= 0 else -0.001
        while target.x != robot.pos.x or target.y != robot.pos.y:
            if (what_to_move == "x" and target.x == robot.pos.x) or \
               (what_to_move == "y" and target.y == robot.pos.y):
               while robot.o != target_rot:
                   robot.o = round(robot.o + i, 3)
                   self.create_path_in_text()
               what_to_move = "x" if what_to_move == "y" else "y"
            if what_to_move == "x":
                robot.pos.x = round(robot.pos.x + x, 3)
            elif what_to_move == "y":
                robot.pos.y = round(robot.pos.y + y, 3)
            self.create_path_in_text()
        if cur_dir:
            self.change_direction(robot, obj, cur_dir, target_dir)

    def rotate(self, robot, r_pos):
        if r_pos == "left" or r_pos == "right":
            degree = 90
        else: degree = 0
        if degree-robot.o >= 0:
            o = 0.001
        else: o = -0.001
        while round(math.radians(degree), 3) != robot.o:
            robot.o = round(robot.o + o, 3)
            self.create_path_in_text()

    def touch_object(self, robot, obj, r_pos):
        if r_pos == "left":
            x, y = 0.001, 0
            target = obj.get_left_side()
        elif r_pos == "right":
            x, y = -0.001, 0
            target = obj.get_right_side()
        if r_pos == "up":
            x, y = 0, -0.001
            target = obj.get_up_side()
        elif r_pos == "down":
            x, y = 0, 0.001
            target = obj.get_down_side()
        while robot.pos.x != target.x or robot.pos.y != target.y:
            robot.pos.x = round(robot.pos.x + x, 3)
            robot.pos.y = round(robot.pos.y + y, 3)
            self.create_path_in_text()
    
    def create_path_in_text(self):
        r_x, r_y = round(self.robot.pos.x, 3), round(self.robot.pos.y, 3)
        r_o = round(self.robot.o, 3)
        tmp = ""
        tmp += "{} {} {}".format(r_x, r_y, r_o)
        for ob in self.obj_list:
            if ob.is_movable:
                o_x, o_y = round(ob.pos.x, 3), round(ob.pos.y, 3)
                tmp += " {} {}".format(o_x, o_y)
        self.path_text += (tmp+"\n")
        if len(self.path_text.split("\n")) > 1000:
            self.generate_path()
            self.path_text = ""

    def generate_path(self):
        file = open(self.filename, "a+")
        file.write(self.path_text)
        file.close()

    def finish(self):
        if len(self.path_text.split("\n")) > 0:
            self.generate_path()
            self.path_text = ""
        file = open(self.filename, "r")
        text = file.read()
        file.close()
        file = open(self.filename, "w")
        file.write(str(len(text.split("\n"))-1)+"\n"+text)
        file.close()
     
class WorldObject:
    def __init__(self, robot=False, goal=False, move=True, obs=False):
        self.is_robot = robot
        self.has_goal = goal
        self.is_movable = move
        self.is_obstacle = obs

class Robot(WorldObject):
    def __init__(self, width, x, y, o):
        super().__init__(robot=True)
        self.width = width
        self.pos = Node(x, y)
        self.o = o
        self.goal = None
        self.path = []

    def get_x1(self, x=None):
        x = self.pos.x if x == None else x
        return round(x - math.cos(self.o) * self.width / 2, 3)

    def get_x2(self, x=None):
        x = self.pos.x if x == None else x
        return round(x + math.cos(self.o) * self.width / 2, 3)

    def get_y1(self, y=None):
        y = self.pos.y if y == None else y
        return round(y - math.sin(self.o) * self.width / 2, 3)

    def get_y2(self, y=None):
        y = self.pos.y if y == None else y
        return round(y + math.sin(self.o) * self.width / 2, 3)
    
class Box(WorldObject):
    def __init__(self, x_init, y_init, width, x_goal, y_goal):
        super().__init__(goal=True)
        self.pos = Node(x_init, y_init)
        self.goal = Node(x_goal, y_goal)
        self.width = width
        self.path = []
        self.goal_found = False

    def get_left_side(self):
        return Node(round(self.pos.x-round(self.width/2, 3), 3), self.pos.y)
    
    def get_right_side(self):
        return Node(round(self.pos.x+round(self.width/2, 3), 3), self.pos.y)
    
    def get_down_side(self):
        return Node(self.pos.x, round(self.pos.y-round(self.width/2, 3), 3))
    
    def get_up_side(self):
        return Node(self.pos.x, round(self.pos.y+round(self.width/2, 3), 3))

class MovableObstacle(WorldObject):
    def __init__(self, x, y, width):
        super().__init__(obs=True)
        self.pos = Node(x, y)
        self.width = width
        self.path = []
        
class StaticObstacle(WorldObject):
    def __init__(self, x1, y1, x2, y2):
        super().__init__(move=False, obs=True)
        self.l_left = Node(x1, y1)
        self.u_right = Node(x2, y2)
        self.x_length = self.u_right.x - self.l_left.x
        self.y_length = self.u_right.y - self.l_left.y
        self.pos = Node(x1 + self.x_length/2, y1 + self.y_length/2)


